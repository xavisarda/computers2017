package es.eug.cfgs.entorns.computer.model;

import es.eug.cfgs.entorns.computer.model.components.Cpu;
import es.eug.cfgs.entorns.computer.model.components.Display;
import es.eug.cfgs.entorns.computer.model.components.Ram;
import es.eug.cfgs.entorns.computer.model.components.Storage;

public class Computer {
	
	private Cpu cpu;
	private Display display;
	private Ram ram;
	private Storage storage;
	
	public Computer(Cpu cpu, Display display, Ram ram, Storage storage) {
		this.cpu = cpu;
		this.display = display;
		this.ram = ram;
		this.storage = storage;
	}
	
	public Computer(String cpuBrand, String cpuModel, Float cpuFreq,
			String disRatio, Float disSize, 
			String ramType, Integer ramCapacity, Float ramFreq,
			String storageType, Integer storageCapacity){
		this.cpu = new Cpu(cpuBrand, cpuModel, cpuFreq);
		this.display = new Display(disRatio, disSize);
		this.ram = new Ram(ramType, ramCapacity, ramFreq);
		this.storage = new Storage(storageType, storageCapacity);
	}
	
	public Cpu getCpu() {
		return cpu;
	}
	public Display getDisplay() {
		return display;
	}
	public Ram getRam() {
		return ram;
	}
	public Storage getStorage() {
		return storage;
	}
	public void setCpu(Cpu cpu) {
		this.cpu = cpu;
	}
	public void setDisplay(Display display) {
		this.display = display;
	}
	public void setRam(Ram ram) {
		this.ram = ram;
	}
	public void setStorage(Storage storage) {
		this.storage = storage;
	}
	
	public String toString(){
		String ret = "** COMPUTER **\n"
				+this.cpu+this.display+this.ram+this.storage;
		return ret;
	}
	
}
