package es.eug.cfgs.entorns.computer.model.components;

public class Cpu {
	
	private String brand;
	private String model;
	private Float freq;
	
	public Cpu(String brand, String model, Float freq) {
		this.brand = brand;
		this.model = model;
		this.freq = freq;
	}
	public String getBrand() {
		return brand;
	}
	public String getModel() {
		return model;
	}
	public Float getFreq() {
		return freq;
	}
	public void setBrand(String brand) {
		this.brand = brand;
	}
	public void setModel(String model) {
		this.model = model;
	}
	public void setFreq(Float freq) {
		this.freq = freq;
	}
	
	public String toString(){
		String ret = "CPU:\n  - Brand: "
				+this.brand+"\n  - Model: "
				+this.model+"\n - Frequency: "
				+this.freq+"GHz\n";
		return ret;
	}
}
