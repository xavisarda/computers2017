package es.eug.cfgs.entorns.computer.model.components;

public class Ram {
	
	private String type;
	private Integer	capacity;
	private Float freq;
	
	public Ram(String type, Integer capacity, Float freq) {
		this.type = type;
		this.capacity = capacity;
		this.freq = freq;
	}
	public String getType() {
		return type;
	}
	public Integer getCapacity() {
		return capacity;
	}
	public Float getFreq() {
		return freq;
	}
	public void setType(String type) {
		this.type = type;
	}
	public void setCapacity(Integer capacity) {
		this.capacity = capacity;
	}
	public void setFreq(Float freq) {
		this.freq = freq;
	}
	
	public String toString(){
		String ret = "RAM:\n  - Type: "
				+this.type+"\n  - Capacity: "
				+this.capacity+"\n - Frequency: "
				+this.freq+"GHz\n";
		return ret;
	}

}
