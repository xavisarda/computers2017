package es.eug.cfgs.entorns.computer.model.components;

public class Display {

	private String ratio;
	private Float size;
	
	public Display(String ratio, Float size) {
		this.ratio = ratio;
		this.size = size;
	}
	public String getRatio() {
		return ratio;
	}
	public Float getSize() {
		return size;
	}
	public void setRatio(String ratio) {
		this.ratio = ratio;
	}
	public void setSize(Float size) {
		this.size = size;
	}
	
	public String toString(){
		String ret = "DISPLAY:\n  - Ratio: "
				+this.ratio+"\n  - Size: "
				+this.size+"\"\n";
		return ret;
	}
}
