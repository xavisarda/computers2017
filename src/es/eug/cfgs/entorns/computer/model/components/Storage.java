package es.eug.cfgs.entorns.computer.model.components;

public class Storage {

	private String type;
	private Integer capacity;
	
	public Storage(String type, Integer capacity) {
		this.type = type;
		this.capacity = capacity;
	}
	public String getType() {
		return type;
	}
	public Integer getCapacity() {
		return capacity;
	}
	public void setType(String type) {
		this.type = type;
	}
	public void setCapacity(Integer capacity) {
		this.capacity = capacity;
	}
	
	public String toString(){
		String ret = "STORAGE:\n  - Type: "
				+this.type+"\n  - Capacity: "
				+this.capacity+"\n";
		return ret;
	}
	
}
