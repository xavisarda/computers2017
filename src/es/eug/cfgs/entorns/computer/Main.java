package es.eug.cfgs.entorns.computer;

import es.eug.cfgs.entorns.computer.model.Computer;

public class Main {

	public static void main(String[] args) {
		 Computer c = new Computer("Intel", "i7", new Float(1.7), 
				 "16:9", new Float(22.1), "DDR3", 4096, 
				 new Float(1.66), "SSD", 2048);
		 
		 System.out.println(c);
	}

}
